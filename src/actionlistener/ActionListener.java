/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package actionlistener;

/**
 *
 * @author Josuè Daniel Roldàn Ochoa
 */


import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JOptionPane;
public class ActionListener {

 
    /**
     * @param args the command line arguments
     */
    
public ActionListener(){  
    
    }
    
private void act(){
JFrame fm= new JFrame();
JButton bt= new JButton("ActionListener");
fm.setLayout(null);
fm.setBounds(400,0,400,250);
fm.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
fm.setLocationRelativeTo(null);
fm.setResizable(false);
JButton bt2= new JButton("Salir");
JTextField fl= new JTextField();
fl.setBounds(124,70,150,20);
bt.setBounds(124,100,150,20);
bt2.setBounds(124,125,150,20);
fm.getContentPane().add(bt);
fm.getContentPane().add(bt2);
fm.getContentPane().add(fl);
    
bt.addActionListener(new java.awt.event.ActionListener(){
public void actionPerformed(ActionEvent e){
JOptionPane.showMessageDialog(null,"Este es una funcion de ActionListener "+"Lo que escribio es: "+fl.getText()+"");
fl.setText(null);
}
});
fl.addActionListener(new java.awt.event.ActionListener(){
public void actionPerformed(ActionEvent e){
JOptionPane.showMessageDialog(null,"Este es una funcion de ActionListener "+"Lo que escribio es: "+fl.getText()+"");
fl.setText(null);
}
});

//Boton Salir
bt2.addActionListener(new java.awt.event.ActionListener(){
public void actionPerformed(ActionEvent e){
System.exit(0);
}
});
fm.setVisible(true);
}
public static void main(String[] args) {
// TODO code application logic here
/** Frame Principal */ 
ActionListener a= new ActionListener();
a.act();
}
}
